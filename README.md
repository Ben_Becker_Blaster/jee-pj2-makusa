# PORTAIL SPORT #
## Benjamin Makusa Master 2 E-service ##


### Configuration ###

* Base de donnée:
Mysql
* Serveur:
Tomcat 8
* Eclipse:
Eclipse Luna

### Installation ###
* #### Initialisation BDD ####
1. User = root Password =  root
2. exécuter le fichier script.sql pour initialiser les tables
* #### Compilation maven ####
1. Après avoir importer le projet maven de bitbucket, faire une compilation maven avec un goal en "clean install"
2. Déployer le projet sur le tomcat 8 avec un "Add & remove"
3. Lancer le serveur et accéder à l'url "http://localhost:{port}/Sport
4. Si des problèmes apparaissent lors de la compilation maven, il est susceptible que les dépendances maven soient mal déployer dans le dossier lib du WEB_INF. Pour y remédier, clique droit sur le projet, Properties->Deployment Assembly . Si le "Maven Dependencies" n'apparait pas dans la liste des "Source", faire Add->Java Build Path Entries -> Maven Dependencies
### Guide d'utilisation ###
* #### Visiteur ####
1. Le visiteur peut accéder au classement des performances par région et par compétition. Le classement est en ordre décroissant avec la meilleurs performances individuel du membre concerner par la compétition ou la région.
2. Le visiteur a accès à la page d'inscription, lors de la validation du formulaire d'inscription, le visiteur reçoit un mail de confirmation de la demande d'inscription envoyer à l'adresse mail référencer. Le visiteur sera dans la liste d'attente pour se faire valider ou refuser l'inscription par l'administrateur du site ou le référent du club concerner.
3. Lorsqu'il aura reçu son mot de passe après la validation de sa demande d'inscription, le visiteur pourra se connecter sur le site dans la page de connexion.
![classement_region.png](https://bitbucket.org/repo/z65br9/images/2300376643-classement_region.png)
* #### Membre ####
1. Après s'être connecter au site en tant que membre. Ce dernier pourra, en plus des fonctionnalité accessible au simple visiteur, renseigner une nouvelle performance dans la page dédier à cet effet. Il pourra choisir le type de performance dans un liste, soit la performance est ponctuelle, soit elle est affilié à une compétition existante. De plus, le visiteur à la possibilité d'ajouter une nouvelle compétition sans recharger la page. La liste des compétitions est remplacée par une case texte où l'on nommera la nouvelle compétition.
2. Après validation, le membre pourra retrouver sa nouvelle performance dans le classement de sa région et de la compétition si la performance n'est pas ponctuelle.
![ajout_performance.png](https://bitbucket.org/repo/z65br9/images/3886665272-ajout_performance.png)
* #### Membre référent de club ####
1. Le membre référent peut accéder la la page de la liste d'attente de son club. Il voit la liste de toute les demandes en attente pour son club ainsi que les refus (grisé dans la liste). La liste est ordonnée en fonction de la date de demande d'inscription d'une manière décroissante.
2. Lorsqu'il clique sur le bouton "Modifier statut", un pop up apparaît en premier plan avec les informations du membre sélectionné. si le référent choisi d'inscrire le membre en attente, ce dernier recevra un mail de confirmation d'inscription avec le mot de passe généré aléatoirement pour accéder au site. Dans le cas d'un refus, le référent à la possibilité de laisser un commentaire expliquant la cause de l'invalidation qui sera transmis au membre par un mail.
![modif_statut.png](https://bitbucket.org/repo/z65br9/images/1622242127-modif_statut.png)
* #### Administrateur ####
1. Pour entrer dans le site en tant qu'administrateur, il faut se connecter avec pour mail "ADMIN" est mot de passe "ADMIN"
2. L'administrateur peut, tout comme le membre référent, accéder à la liste des demande en attente. Cependant, il voit uniquement afficher les demandes en attentes (il ne peut pas voir les refus) de tout les clubs confondu.
3. L'administrateur peut modifier le membre référent d'un club. Il choisit un club et accède à la liste de tout les membres inscrit dans le club. Le membre référent actuel du club sélectionner est marqué par un fond bleu dans la liste. il peut changer de référent est la liste est mise à jour après validation. Il n'y a qu'un seul membre référent par club.
![membre_referent.png](https://bitbucket.org/repo/z65br9/images/256262970-membre_referent.png)
* #### Info complémentaire ####
1. J'ai créer une boîte mail pour le projet nommée "master2eservicemakusa@gmail.com" et avec pour mot de passe "Patlabor13". C'est par le biais de ce mail que spring fera l'envoye de message, vous pourrez y trouver les accusés de réception des messages envoyer.
2. Il est possible que le serveur stoppe à cause d'une exception "PermGen Space". Pour y remédier, il suffit de redémarrer le serveur et de rafraîchir la page avec la touche "f5"