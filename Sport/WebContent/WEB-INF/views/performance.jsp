<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />



<head>
<body>


	<div class="container">

		<h1>Ajout d'une performance</h1>

		<div class="alert alert-success" role="alert" id="alert" style="display: none" ><span>Bravo!</span>La performance a bien été ajouter</div>
		<form id="integerForm" class="form-horizontal"
			action="${pageContext.request.contextPath}/performance" method="POST">
			<div class="form-group">
				<label for="nom">Date</label> <input
					class="form-control date-picker" required="required" min="4"
					placeholder="Date de la performance" name="date" />
			</div>

			<div class="form-group">
				<label for="prenom">Points</label> <input class="form-control"
					required="required" min="1" placeholder="Points"
					data-bind="value:replyNumber" name="points" />
			</div>

			<div class="form-group" id="existCompetition">
				<label for="competition">Compétition</label> <select class="form-control"
					name="competition">
					<option value="0">Performance ponctuelle</option>
					<c:forEach items="${competitions}" var="competition">
						<option value="${competition.id}">${competition}</option>
					</c:forEach>
				</select>
			</div>
			
			<div class="form-group" id="newCompetition" style="display: none">
				<label for="newcompetition">Nouvelle compétition</label> <input
					id="newC" class="form-control"  min="1"
					placeholder="Ecrire le nom de la compétition" name="newcompetition" />
			</div>

			<div class="checkbox">
				<label><input id="checkbox" type="checkbox" name="newComp"
					onclick="change()">Ajouter une nouvelle compétition</label>
			</div>

			<div class="form-group">
				<input class="btn btn-default" type="submit"
					value="Enregistrer performance" />
			</div>
		</form>

	</div>
	<!-- /container -->

	<script>
		$(document).ready(function() {
			$('#integerForm').formValidation();
			
			if("${ajouter}"){
				document.getElementById("alert").style.display = "block";
			}else{
				document.getElementById("alert").style.display = "none";
			}
			
		});

		function change() {
			if (document.getElementById("checkbox").checked) {
				document.getElementById("existCompetition").style.display = "none";
				document.getElementById("newCompetition").style.display = "block";
			} else {
				document.getElementById("existCompetition").style.display = "block";
				document.getElementById("newCompetition").style.display = "none";
				document.getElementById("newC").value = "";
			}
		}
	</script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>