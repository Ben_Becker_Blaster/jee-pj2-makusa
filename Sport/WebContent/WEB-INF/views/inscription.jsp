<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />

<script type="text/javascript">
$(document).ready(function(){
	//if("${message}"){
	//	alert("${message}");
	//}
	
});
</script>

<head>

<body>


	<div class="container">

		<h1>Inscription</h1>
	
		
		<form:form class="form-horizontal" modelAttribute="membre">
			<div class="form-group">
				<label for="nom">Nom</label>
				<form:input class="form-control" required="required" minlength="2"
					placeholder="Nom" path="nom" />
			</div>
			<div class="form-group">
				<label for="prenom">Pr�nom</label>
				<form:input class="form-control" required="required" minlength="2"
					placeholder="Pr�nom" path="prenom" />
			</div>

			<div class="form-group">
				<label for="email">Email</label>
				<form:input class="form-control" required="required" minlength="4"
					placeholder="Email" path="email" />
			</div>

			<div class="form-group">
				<label for="prenom">Naissance</label>
				<form:input class="form-control date-picker" required="required"
					minlength="4" placeholder="Date de naissance" path="date" />
			</div>

			<div class="form-group">
				<label for="club">Club</label>

				<form:select path="club">
					<c:forEach items="${clubs}" var="club">
						<form:option value="${club.id}">${club}</form:option>
					</c:forEach>
				</form:select>
			</div>

			<div class="form-group">
				<input class="btn btn-default" type="submit" value="Enregistrer" />
			</div>
		</form:form>


	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>