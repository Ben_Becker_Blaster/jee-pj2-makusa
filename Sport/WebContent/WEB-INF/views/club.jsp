<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />
<body>


	<div class="container">

		<h1>Liste d'attente</h1>
		<h3>
			Club :
			<c:out value="${sessionScope.membre.club.nom}" />
		</h3>


		<table class="table">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Pr�nom</th>
					<th>Date</th>
					<th>Statut</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${membres}" var="membre">
					<c:if test="${membre.etat =='REFUSER'}">
						<tr style="background-color: grey">
					</c:if>
					<c:if test="${membre.etat !='REFUSER'}">
						<tr>
					</c:if>

					<td>${membre.nom}</td>
					<td>${membre.prenom}</td>
					<td>${membre.date}</td>
					<td>${membre.etat}</td>
					<td><button type="button"
							class="btn btn-primary btn-lg status"
							idmembre="<c:out value="${membre.id}"/>"
							nom="<c:out value="${membre.nom}"/>"
							prenom="<c:out value="${membre.prenom}"/>"
							date="<c:out value="${membre.date}"/>">Status</button></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>



		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">

					<form id="integerForm" class="form-horizontal"
						action="${pageContext.request.contextPath}/club/membres"
						method="POST">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Modification de
								status</h4>
						</div>
						<div class="modal-body">
							<h2 id="nomMembre"></h2>
							<h3 id="dateMembre"></h3>
							<input id="idMembre" type="text"
								style="display: none" class="form-control" required="required"
								placeholder="Mot de passe" name="idMembre" /> <label for="etat">Statut
								membre</label> <select class="form-control" name="etat" id="change" onchange="addComment()">
								<option value="INSCRIT">INSCRIT</option>
								<option value="REFUSER">REFUSER</option>
							</select>

							<div class="form-group" id="com" style="display:none">
								<label for="comment">Laisser un commentaire:</label>
								<textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary"
								value="Modifier status" />
						</div>

					</form>

				</div>
			</div>
		</div>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

		});

		$(document).on(
				"click",
				".status",
				function() {

					var idmembre = $(this).attr("idmembre");
					var nom = $(this).attr("nom");
					var prenom = $(this).attr("prenom");
					var date = $(this).attr("date");

					document.getElementById("nomMembre").innerHTML = prenom
							+ " " + nom;
					document.getElementById("dateMembre").innerHTML = date;

					document.getElementById("idMembre").setAttribute("value",
							idmembre);
					$("#myModal").modal("show");
				});
		
		function addComment(){
			if(document.getElementById("change").value == "REFUSER"){
				document.getElementById("com").style.display = "block";
			}else{
				document.getElementById("comment").value = "";
				document.getElementById("com").style.display = "none";
			}		
		}
	</script>

</body>
</html>