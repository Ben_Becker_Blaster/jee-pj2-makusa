<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<c:url value="/" />">Sport</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       <c:if test="${(empty sessionScope.membre)}">
       
            <li><a href="<c:url value='/inscription' />">Inscription </a></li>
             <li><a href="<c:url value='/connexion' />">Connexion</a></li>
            
       </c:if>
         <c:if test="${(not empty sessionScope.membre)}">
             <li><a href="<c:url value='/deconnexion' />">Deconnexion</a></li>
       </c:if>
         <c:if test="${(not empty sessionScope.membre) and (sessionScope.membre.etat ne 'ADMIN')}">
             <li><a href="<c:url value='/performance' />">Performances</a></li>
       </c:if>
     
          <li><a href="<c:url value='/classement/region' />">Classement Regional</a></li>
          <li><a href="<c:url value='/classement/competition' />">Classement Competition</a></li>
          
         <c:if test="${(not empty sessionScope.membre) and (sessionScope.membre.club.referent.id eq sessionScope.membre.id) and (sessionScope.membre.etat ne 'ADMIN')}">
         	<li><a href="<c:url value='/club/membres' />">Liste d'attente</a></li>
          </c:if>
          <c:if test="${(not empty sessionScope.membre) and (sessionScope.membre.club.referent.id ne sessionScope.membre.id) and (sessionScope.membre.etat eq 'ADMIN')}">
         	<li><a href="<c:url value='/club/membresadmin' />">Liste d'attente</a></li>
          </c:if>
           <c:if test="${(not empty sessionScope.membre) and (sessionScope.membre.etat eq 'ADMIN')}">
             <li><a href="<c:url value='/membre/referent' />">Membre r�f�rent</a></li>
       </c:if>
       
       
      </ul>
     
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>