<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />
<body>


	<div class="container">
		<h1>Connexion</h1>

		<c:if test="${not empty erreur}">
			<div class="alert alert-danger" role="alert">${erreur}</div>
		</c:if>

		<form class="form-horizontal"
			action="${pageContext.request.contextPath}/connexion" method="POST">
			<div class="form-group">
				<label for="email">Email</label> <input type="text"
					class="form-control" required="required" placeholder="Adresse mail"
					name="email" />
			</div>

			<div class="form-group">
				<label for="mdp">Mot de passe</label> <input type="text"
					class="form-control" required="required" placeholder="Mot de passe"
					name="mdp" />
			</div>

			<div class="form-group">
				<input class="btn btn-default" type="submit" value="Connexion" />
			</div>
		</form>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>