<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />
<body>


	<div class="container">


		<h1>Classement comptition</h1>

		<form class="form-horizontal"
			action="${pageContext.request.contextPath}/classement/competition"
			method="POST">
			<label for="prenom">Competition</label> <select name="id"  class="form-control">
				<c:forEach items="${competitions}" var="competition">
					<option value="${competition.id}">${competition.nom}</option>
				</c:forEach>
			</select> <input class="btn btn-default" type="submit" value="Selectionner" />
		</form>

		<c:if test="${comp != null}">
			<h2>${comp.nom}</h2>
		</c:if>
		<c:if test="${comp == null}">
			<h2>Choisir une compétition</h2>
		</c:if>


		<table class="table">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Date</th>
					<th>Score</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${performances}" var="performance">
					<tr>
						<td>${performance.membre.nom}</td>
						<td>${performance.membre.prenom}</td>
						<td>${performance.date}</td>
						<td>${performance.points}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>