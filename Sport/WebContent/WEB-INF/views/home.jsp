<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />
<body>


	<div class="container">

	

		<h1>Bienvenue sur le portail sportif</h1>

		<p>Circa hos dies Lollianus primae lanuginis adulescens, Lampadi
			filius ex praefecto, exploratius causam Maximino spectante, convictus
			codicem noxiarum artium nondum per aetatem firmato consilio
			descripsisse, exulque mittendus, ut sperabatur, patris inpulsu
			provocavit ad principem, et iussus ad eius comitatum duci, de fumo,
			ut aiunt, in flammam traditus Phalangio Baeticae consulari cecidit
			funesti carnificis manu.</p>

		<p>Vita est illis semper in fuga uxoresque mercenariae conductae
			ad tempus ex pacto atque, ut sit species matrimonii, dotis nomine
			futura coniunx hastam et tabernaculum offert marito, post statum diem
			si id elegerit discessura, et incredibile est quo ardore apud eos in
			venerem uterque solvitur sexus.</p>

		<p>Etenim si attendere diligenter, existimare vere de omni hac
			causa volueritis, sic constituetis, iudices, nec descensurum quemquam
			ad hanc accusationem fuisse, cui, utrum vellet, liceret, nec, cum
			descendisset, quicquam habiturum spei fuisse, nisi alicuius
			intolerabili libidine et nimis acerbo odio niteretur. Sed ego
			Atratino, humanissimo atque optimo adulescenti meo necessario,
			ignosco, qui habet excusationem vel pietatis vel necessitatis vel
			aetatis. Si voluit accusare, pietati tribuo, si iussus est,
			necessitati, si speravit aliquid, pueritiae. Ceteris non modo nihil
			ignoscendum, sed etiam acriter est resistendum.</p>

		<p>Post hoc impie perpetratum quod in aliis quoque iam timebatur,
			tamquam licentia crudelitati indulta per suspicionum nebulas
			aestimati quidam noxii damnabantur. quorum pars necati, alii puniti
			bonorum multatione actique laribus suis extorres nullo sibi relicto
			praeter querelas et lacrimas, stipe conlaticia victitabant, et civili
			iustoque imperio ad voluntatem converso cruentam, claudebantur
			opulentae domus et clarae.</p>

		<p>Post quorum necem nihilo lenius ferociens Gallus ut leo
			cadaveribus pastus multa huius modi scrutabatur. quae singula narrare
			non refert, me professione modum, quod evitandum est, excedamus.</p>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>