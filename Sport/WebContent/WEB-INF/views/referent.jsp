<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />
<body>


	<div class="container">



		<h1>Membre r�f�rent</h1>

		<form class="form-horizontal"
			action="${pageContext.request.contextPath}/membre/referent"
			method="POST">
			<label for="club">Club</label> <select name="id" class="form-control">
				<c:forEach items="${clubs}" var="club">
					<option value="${club.id}">${club.nom}</option>
				</c:forEach>
			</select> <input class="btn btn-default" type="submit" value="Selectionner" />
		</form>

		<c:if test="${clu != null}">
			<h2>${clu.nom}</h2>
		</c:if>
		<c:if test="${clu == null}">
			<h2>Choisir un club sportif</h2>
		</c:if>


		<table class="table">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Pr�nom</th>
					<th>Date</th>
					<th>email</th>
					<th></th>

				</tr>
			</thead>
			<tbody>

				<c:forEach items="${membres}" var="membre">
					<c:if test="${membre.id == clu.referent.id}">
						<tr style="background-color: #00CCFF">
					</c:if>
					<c:if test="${membre.id != clu.referent.id}">
						<tr>
					</c:if>
					<form id="integerForm" class="form-horizontal"
						action="${pageContext.request.contextPath}/membre/referent2"
						method="POST">
						<td>${membre.nom}</td>
						<td>${membre.prenom}</td>
						<td>${membre.date}</td>
						<td>${membre.email}</td>
						<td><input value="<c:out value="${membre.id}" />"
							name="idref" style="display: none" />
							<input value="<c:out value="${clu.id}" />"
							name="idclu" style="display: none" />
							 <input type="submit"
							class="btn btn-primary" value="Modifier r�f�rent" /></td>
					</form>
					</tr>
				</c:forEach>

			</tbody>
		</table>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>