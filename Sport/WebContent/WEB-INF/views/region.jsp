<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../views/includes/header.jsp" />
<body>


	<div class="container">



		<h1>Classement regional</h1>

		<form class="form-horizontal"
			action="${pageContext.request.contextPath}/classement/region"
			method="POST">
			<label for="prenom">Region</label> <select name="id" class="form-control">
				<c:forEach items="${regions}" var="region">
					<option value="${region.id}">${region.nom}</option>
				</c:forEach>
			</select> <input class="btn btn-default" type="submit" value="Selectionner" />
		</form>

		<c:if test="${reg != null}">
			<h2>${reg.nom}</h2>
		</c:if>
		<c:if test="${reg == null}">
			<h2>Choisir une r�gion</h2>
		</c:if>


		<table class="table">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Pr�nom</th>
					<th>Date</th>
					<th>Score</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${performances}" var="performance">
					<tr>
						<td>${performance.membre.nom}</td>
						<td>${performance.membre.prenom}</td>
						<td>${performance.date}</td>
						<td>${performance.points}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>