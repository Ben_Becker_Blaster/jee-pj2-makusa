package com.portailsport.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

import com.hibernate.util.HibernateUtil;
import com.portailsport.dao.RegionDao;
import com.portailsport.model.Region;


public class RegionDaoImpl implements RegionDao {

	
	public void stop(){
		Session session = HibernateUtil.currentSession();
		session.getTransaction().commit();
		HibernateUtil.closeSession();
	}

	public void start(){
		HibernateUtil.buildSessionFactory();
		Session session = HibernateUtil.currentSession();
		session.beginTransaction();
	}
	
	@SuppressWarnings("unchecked")
	public List<Region> listRegions() {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Region.class);
		return (List<Region>) criteria.list();
	}

	public void createRegion(String nom) {
		Session session = HibernateUtil.currentSession();
		Region region = new Region();
		region.setNom(nom);
		session.save(region);
	}

	@Override
	public Region getRegion(int id) {
		Session session = HibernateUtil.currentSession();
		Region res = (Region)session.get(Region.class, id);
		return res;
	}

}
