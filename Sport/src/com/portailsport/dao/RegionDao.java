package com.portailsport.dao;

import java.util.List;

import com.portailsport.model.Region;

public interface RegionDao {
	public void start();
	public void stop();
	public List<Region> listRegions();
	public void createRegion(String nom);
	public Region getRegion(int id);
}
