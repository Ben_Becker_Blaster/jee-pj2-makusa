package com.portailsport.dao;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hibernate.util.HibernateUtil;
import com.portailsport.dao.PerformanceDao;
import com.portailsport.model.Competition;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;
import com.portailsport.model.Region;

public class PerformanceDaoImpl implements PerformanceDao {
	
	public void stop(){
		Session session = HibernateUtil.currentSession();
		session.getTransaction().commit();
		HibernateUtil.closeSession();
	}

	public void start(){
		HibernateUtil.buildSessionFactory();
		Session session = HibernateUtil.currentSession();
		session.beginTransaction();
	}
	
	public void createPerformance(Calendar date, int points,
			Competition competition, Membre membre) {
		Session session = HibernateUtil.currentSession();
		Performance performance = new Performance();
		performance.setDate(date);
		performance.setPoints(points);
		performance.setCompetition(competition);
		performance.setMembre(membre);
		session.save(performance);
	}

	public void updatePerformance(Performance performance) {
		Session session = HibernateUtil.currentSession();
		session.update(performance);
	}

	public void deletePerformance(Performance performance) {
		Session session = HibernateUtil.currentSession();
		session.delete(performance);
	}
	
	@SuppressWarnings("unchecked")
	public List<Performance> listPerformances() {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Performance.class);
		return (List<Performance>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Performance> listPerformances(Region region) {
		Session session = HibernateUtil.currentSession();
		Query query = session.createQuery(" FROM Performance performance WHERE performance.membre IN (FROM Membre membre WHERE membre.club IN (FROM Club club WHERE club.region = ALL (FROM Region region WHERE region.id = :id))) ORDER BY performance.points DESC" );
		query.setParameter("id",region.getId());
		return (List<Performance>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Performance> listPerformances(Competition competition) {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Performance.class);
		Criterion comp = Restrictions.eq("competition", competition);
		criteria.add(comp); 
		criteria.addOrder(Order.desc("points"));
		return (List<Performance>) criteria.list();
	}

}
