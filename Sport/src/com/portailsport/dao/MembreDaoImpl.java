package com.portailsport.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hibernate.util.HibernateUtil;
import com.portailsport.dao.MembreDao;
import com.portailsport.model.Club;
import com.portailsport.model.Competition;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;

public class MembreDaoImpl implements MembreDao {
	
	public void stop(){
		Session session = HibernateUtil.currentSession();
		session.getTransaction().commit();
		HibernateUtil.closeSession();
	}

	public void start(){
		HibernateUtil.buildSessionFactory();
		Session session = HibernateUtil.currentSession();
		session.beginTransaction();
	}

	
	public void createMembre(String nom, String prenom, String email,
			Calendar naissance, Club club) {
		Session session = HibernateUtil.currentSession();
		Membre membre = new Membre();
		membre.setNom(nom);
		membre.setPrenom(prenom);
		membre.setEmail(email);
		membre.setDate(naissance);
		membre.setClub(club);
		session.save(membre);
	}

	public Membre auth(String email, String password) {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Membre.class);
		Criterion mail = Restrictions.eq("email", email);
		Criterion mdp = Restrictions.eq("mdp", password);
		criteria.add(mail);
		criteria.add(mdp);
		Membre res = (Membre) criteria.uniqueResult();
		return res;
	}

	public void updateMembre(Membre membre) {
		Session session = HibernateUtil.currentSession();
		session.update(membre);
	
	}

	public void deleteMembre(Membre membre) {
		Session session = HibernateUtil.currentSession();
		session.delete(membre);

	}

	@SuppressWarnings("unchecked")
	public List<Membre> listMembres() {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Membre.class);
		return (List<Membre>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Membre> listMembres(Competition competition) {
		Session session = HibernateUtil.currentSession();	
		Criteria criteria = session.createCriteria(Performance.class);
		Criterion comp = Restrictions.eq("competition", competition);
		criteria.add(comp); 
		List<Performance> list = (List<Performance>) criteria.list();
		List<Membre> res = new ArrayList<Membre>();
		for(Performance p : list){
			res.add(p.getMembre());
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Membre> listMembres(Club club) {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Membre.class);
		Criterion clu = Restrictions.eq("club", club);
		criteria.add(clu); 
		criteria.addOrder(Order.desc("date"));
		return (List<Membre>) criteria.list();
	}

	public Membre getMembre(Performance performance) {
		return performance.getMembre();
	}

	public void createMembre(Membre membre) {
		Session session = HibernateUtil.currentSession();
		session.save(membre);
	}

	@Override
	public Membre getMembre(int id) {
		Session session = HibernateUtil.currentSession();
		Membre res = (Membre) session.get(Membre.class, id);
		return res;
	}

}
