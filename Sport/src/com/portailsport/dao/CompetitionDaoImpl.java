package com.portailsport.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

import com.hibernate.util.HibernateUtil;
import com.portailsport.dao.CompetitionDao;
import com.portailsport.model.Competition;
import com.portailsport.model.Performance;
import com.portailsport.model.Region;

public class CompetitionDaoImpl implements CompetitionDao {
	
	public void stop(){
		Session session = HibernateUtil.currentSession();
		session.getTransaction().commit();
		HibernateUtil.closeSession();
	}

	public void start(){
		HibernateUtil.buildSessionFactory();
		Session session = HibernateUtil.currentSession();
		session.beginTransaction();
	}
	
	public void createCompetition(String nom) {
		Session session = HibernateUtil.currentSession();
		Competition competition = new Competition();
		competition.setNom(nom);
		session.save(competition);
	}

	@SuppressWarnings("unchecked")
	public List<Competition> listCompetitions() {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Competition.class);
		return (List<Competition>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Competition> listCompetitions(Region region) {
		Session session = HibernateUtil.currentSession();
		Query query = session.createQuery(" FROM Performance performance WHERE performance.membre IN (FROM Membre membre WHERE membre.club IN (FROM Club club WHERE club.region = ALL (FROM Region region WHERE region.id = :id)))");
		query.setParameter("id", region.getId());
		List<Performance> list = query.list();
		List<Competition> res = new ArrayList<Competition>();
		for(Performance p : list){
			if(p.getCompetition() !=null && !res.contains(p.getCompetition())){
				res.add(p.getCompetition());
			}
		}
		return res;
	}

	public void updateCompetition(Competition competition) {
		Session session = HibernateUtil.currentSession();
		session.update(competition);
	}

	public void deleteCompetition(Competition competition) {
		Session session = HibernateUtil.currentSession();
		session.delete(competition);
	}

	@Override
	public Competition getCompetition(int id) {
		Session session = HibernateUtil.currentSession();
		Competition res = (Competition) session.get(Competition.class, id);
		return res;
	}

	@SuppressWarnings("unchecked")
	public Competition getCompetition(String nom) {
		Session session = HibernateUtil.currentSession();
		Query query = session.createQuery(" FROM Competition competition WHERE competition.nom = :nom");
		query.setParameter("nom", nom);
		List<Competition> list = query.list();
		if (list.size()<1){
			return null;
		} else{
			return list.get(0);
		}
		
	}

}
