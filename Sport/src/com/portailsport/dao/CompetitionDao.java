package com.portailsport.dao;

import java.util.List;

import com.portailsport.model.Competition;
import com.portailsport.model.Region;

public interface CompetitionDao {
	public void start();
	public void stop();
	public void createCompetition(String nom);
	public List<Competition> listCompetitions();
	public List<Competition> listCompetitions(Region region);
	public void updateCompetition(Competition competition);
	public void deleteCompetition(Competition competition);
	public Competition getCompetition(int id);
	public Competition getCompetition(String nom);
}
