package com.portailsport.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.hibernate.util.HibernateUtil;
import com.portailsport.dao.ClubDao;
import com.portailsport.model.Club;
import com.portailsport.model.Region;

public class ClubDaoImpl implements ClubDao {
	
	public void stop(){
		Session session = HibernateUtil.currentSession();
		session.getTransaction().commit();
		HibernateUtil.closeSession();
	}

	public void start(){
		HibernateUtil.buildSessionFactory();
		Session session = HibernateUtil.currentSession();
		session.beginTransaction();
	}

	public void updateClub(Club club) {	
		Session session = HibernateUtil.currentSession();
		session.update(club);
	}

	@SuppressWarnings("unchecked")
	public List<Club> listClubs() {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Club.class);
		criteria.addOrder(Order.asc("nom"));
		return (List<Club>) criteria.list();	
	}

	@SuppressWarnings("unchecked")
	public List<Club> listClubs(Region region) {
		Session session = HibernateUtil.currentSession();
		Criteria criteria = session.createCriteria(Club.class);
		Criterion reg = Restrictions.eq("region", region);
		criteria.add(reg); 
		criteria.addOrder(Order.asc("nom"));
		return (List<Club>) criteria.list();
	}

	@Override
	public Club getClub(int id) {
		Session session = HibernateUtil.currentSession();
		Club club = (Club)session.get(Club.class, id);
		return club;
	}

}
