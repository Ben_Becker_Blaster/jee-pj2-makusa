package com.portailsport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portailsport.dao.CompetitionDao;
import com.portailsport.model.Competition;
import com.portailsport.model.Region;

@Service("competitionService")
@Transactional
public class CompetitionServiceImpl implements CompetitionService {

	@Autowired
	private CompetitionDao competitionDao;
	
	@Override
	public void createCompetition(String nom) {
		competitionDao.start();
		competitionDao.createCompetition(nom);
		competitionDao.stop();
	}

	@Override
	public List<Competition> listCompetitions() {
		competitionDao.start();
		List<Competition> res = competitionDao.listCompetitions();
		competitionDao.stop();
		return res;
	}

	@Override
	public List<Competition> listCompetitions(Region region) {
		competitionDao.start();
		List<Competition> res = competitionDao.listCompetitions(region);
		competitionDao.stop();
		return res;
	}

	@Override
	public void updateCompetition(Competition competition) {
		competitionDao.start();
		competitionDao.updateCompetition(competition);
		competitionDao.stop();
	}

	@Override
	public void deleteCompetition(Competition competition) {
		competitionDao.start();
		competitionDao.deleteCompetition(competition);
		competitionDao.stop();
	}

	@Override
	public Competition getCompetition(int id) {
		competitionDao.start();
		Competition res = competitionDao.getCompetition(id);
		competitionDao.stop();
		return res;
	}

	@Override
	public Competition getCompetition(String nom) {
		competitionDao.start();
		Competition res = competitionDao.getCompetition(nom);
		competitionDao.stop();
		return res;
	}

}
