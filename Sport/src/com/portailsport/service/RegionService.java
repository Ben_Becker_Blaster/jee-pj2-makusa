package com.portailsport.service;

import java.util.List;

import com.portailsport.model.Region;

public interface RegionService {

	public List<Region> listRegions();
	public void createRegion(String nom);
	public Region getRegion(int id);
}
