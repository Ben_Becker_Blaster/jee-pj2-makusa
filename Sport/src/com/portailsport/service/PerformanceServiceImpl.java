package com.portailsport.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portailsport.dao.PerformanceDao;
import com.portailsport.model.Competition;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;
import com.portailsport.model.Region;

@Service("performanceService")
@Transactional
public class PerformanceServiceImpl implements PerformanceService {

	@Autowired
	private PerformanceDao performanceDao;
	
	@Override
	public void createPerformance(Calendar date, int points,
			Competition competition, Membre membre) {
		performanceDao.start();
		performanceDao.createPerformance(date, points, competition, membre);
		performanceDao.stop();
	}

	@Override
	public void updatePerformance(Performance performance) {
		performanceDao.start();
		performanceDao.updatePerformance(performance);
		performanceDao.stop();
	}

	@Override
	public void deletePerformance(Performance performance) {
		performanceDao.start();
		performanceDao.deletePerformance(performance);
		performanceDao.stop();
	}

	@Override
	public List<Performance> listPerformances() {
		performanceDao.start();
		List<Performance> res = performanceDao.listPerformances();
		performanceDao.stop();
		return res;
	}

	@Override
	public List<Performance> listPerformances(Region region) {
		performanceDao.start();
		List<Performance> res = performanceDao.listPerformances(region);
		performanceDao.stop();
		return res;
	}

	@Override
	public List<Performance> listPerformances(Competition competition) {
		performanceDao.start();
		List<Performance> res = performanceDao.listPerformances(competition);
		performanceDao.stop();
		return res;
	}

}
