package com.portailsport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portailsport.dao.ClubDao;
import com.portailsport.model.Club;
import com.portailsport.model.Region;

@Service
@Transactional
public class ClubServiceImpl implements ClubService {

	@Autowired
	private ClubDao clubDao;
	
	@Override
	public void updateClub(Club club) {
		clubDao.start();
		clubDao.updateClub(club);
		clubDao.stop();
	}

	@Override
	public List<Club> listClubs() {
		clubDao.start();
		List<Club> res = clubDao.listClubs();
		clubDao.stop();
		return res;
	}

	@Override
	public List<Club> listClubs(Region region) {
		clubDao.start();
		List<Club> res = clubDao.listClubs(region);
		clubDao.stop();
		return res;
	}

	@Override
	public Club getClub(int id) {
		clubDao.start();
		Club club = clubDao.getClub(id);
		clubDao.stop();
		return club;
	}

}
