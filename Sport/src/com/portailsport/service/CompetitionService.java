package com.portailsport.service;

import java.util.List;

import com.portailsport.model.Competition;
import com.portailsport.model.Region;

public interface CompetitionService {

	public void createCompetition(String nom);
	public List<Competition> listCompetitions();
	public List<Competition> listCompetitions(Region region);
	public void updateCompetition(Competition competition);
	public void deleteCompetition(Competition competition);
	public Competition getCompetition(int id);
	public Competition getCompetition(String nom);
}
