package com.portailsport.service;

import java.util.Calendar;
import java.util.List;

import com.portailsport.model.Club;
import com.portailsport.model.Competition;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;

public interface MembreService {

	public void createMembre(String nom, String prenom, String email, Calendar naissance, Club club);
	public void createMembre(Membre membre);
	public Membre auth(String email, String password);
	public void updateMembre(Membre membre);
	public void deleteMembre(Membre membre);
	public List<Membre> listMembres();
	public List<Membre> listMembres(Competition competition);
	public List<Membre> listMembres(Club club);
	public Membre getMembre(Performance performance);
	public Membre getMembre(int id);
}
