package com.portailsport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portailsport.dao.RegionDao;
import com.portailsport.model.Region;

@Service("regionService")
@Transactional
public class RegionserviceImpl implements RegionService {

	@Autowired
	private RegionDao regionDao;
	
	@Override
	public List<Region> listRegions() {
		regionDao.start();
		List<Region> res = regionDao.listRegions();
		regionDao.stop();
		return res;
	}

	@Override
	public void createRegion(String nom) {
		regionDao.start();
		regionDao.createRegion(nom);
		regionDao.stop();
	}

	@Override
	public Region getRegion(int id) {
		regionDao.start();
		Region res = regionDao.getRegion(id);
		regionDao.stop();
		return res;
	}

}
