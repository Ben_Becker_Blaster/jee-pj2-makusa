package com.portailsport.service;

import java.util.List;

import com.portailsport.model.Club;
import com.portailsport.model.Region;

public interface ClubService {

	public void updateClub(Club club);
	public List<Club> listClubs();
	public List<Club> listClubs(Region region);
	public Club getClub(int id);
}
