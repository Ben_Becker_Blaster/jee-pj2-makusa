package com.portailsport.service;

import java.util.Calendar;
import java.util.List;

import com.portailsport.model.Competition;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;
import com.portailsport.model.Region;

public interface PerformanceService {

	public void createPerformance(Calendar date, int points, Competition competition, Membre membre);
	public void updatePerformance(Performance performance);
	public void deletePerformance(Performance performance);
	public List<Performance> listPerformances();
	public List<Performance> listPerformances(Region region);
	public List<Performance> listPerformances(Competition competition);
}
