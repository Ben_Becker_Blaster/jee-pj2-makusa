package com.portailsport.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portailsport.dao.MembreDao;
import com.portailsport.model.Club;
import com.portailsport.model.Competition;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;

@Service("membreService")
@Transactional
public class MembreServiceImpl implements MembreService {

	@Autowired
	private MembreDao membreDao;
	
	@Override
	public void createMembre(String nom, String prenom, String email,
			Calendar naissance, Club club) {
		membreDao.start();
		membreDao.createMembre(nom, prenom, email, naissance, club);	
		membreDao.stop();
	}

	@Override
	public Membre auth(String email, String password) {
		membreDao.start();
		Membre res = membreDao.auth(email, password);
		membreDao.stop();
		return res;
	}

	@Override
	public void updateMembre(Membre membre) {
		membreDao.start();
		membreDao.updateMembre(membre);	
		membreDao.stop();
	}

	@Override
	public void deleteMembre(Membre membre) {
		membreDao.start();
		membreDao.deleteMembre(membre);	
		membreDao.stop();
	}

	@Override
	public List<Membre> listMembres() {
		membreDao.start();
		List<Membre> res =membreDao.listMembres();	
		membreDao.stop();
		return res;
	}

	@Override
	public List<Membre> listMembres(Competition competition) {
		membreDao.start();
		List<Membre> res =membreDao.listMembres(competition);	
		membreDao.stop();
		return res;
	}

	@Override
	public List<Membre> listMembres(Club club) {
		membreDao.start();
		List<Membre> res =membreDao.listMembres(club);	
		membreDao.stop();
		return res;
	}

	@Override
	public Membre getMembre(Performance performance) {
		membreDao.start();
		Membre res =membreDao.getMembre(performance);	
		membreDao.stop();
		return res;
	}

	@Override
	public void createMembre(Membre membre) {
		membreDao.start();
		membreDao.createMembre(membre);	
		membreDao.stop();
	}

	@Override
	public Membre getMembre(int id) {
		membreDao.start();
		Membre res =membreDao.getMembre(id);	
		membreDao.stop();
		return res;
	}

}
