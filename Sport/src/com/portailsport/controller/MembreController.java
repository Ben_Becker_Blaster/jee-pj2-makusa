package com.portailsport.controller;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.Ostermiller.util.RandPass;
import com.portailsport.bean.MembreBean;
import com.portailsport.bean.PerformanceBean;
import com.portailsport.model.Club;
import com.portailsport.model.Competition;
import com.portailsport.model.EtatMembre;
import com.portailsport.model.Membre;
import com.portailsport.model.Performance;
import com.portailsport.model.Region;
import com.portailsport.service.ClubService;
import com.portailsport.service.CompetitionService;
import com.portailsport.service.MembreService;
import com.portailsport.service.PerformanceService;
import com.portailsport.service.RegionService;

@Controller
@RequestMapping("/")
public class MembreController {

	private static final Logger logger = LoggerFactory
			.getLogger(MembreController.class);
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private RandPass randPass = new RandPass(new char[] { 'a', 'b', 'c', 'd',
			'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
			'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' });
	private RandPass randPass2 = new RandPass(new char[] { '_', '-', '@', '#',
			'&', '(', '!', '?', ')', '$', '%', '?', ':', ';', '/', '.', '?',
			',' });
	private RandPass randPass3 = new RandPass(new char[] { 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' });

	@Autowired
	ClubService clubService;

	@Autowired
	MembreService membreService;

	@Autowired
	RegionService regionService;

	@Autowired
	PerformanceService performanceService;

	@Autowired
	CompetitionService competitionService;

	public String randomMdp() {
		String res1 = randPass.getPass(8);
		String res2 = randPass2.getPass(1);
		String res3 = randPass3.getPass(1);
		int res4 = (int) (Math.random() * 9);

		String res = shuffle(res1 + res2 + res3 + res4);
		return res;
	}

	public String shuffle(String input) {
		List<Character> characters = new ArrayList<Character>();
		for (char c : input.toCharArray()) {
			characters.add(c);
		}
		StringBuilder output = new StringBuilder(input.length());
		while (characters.size() != 0) {
			int randPicker = (int) (Math.random() * characters.size());
			output.append(characters.remove(randPicker));
		}
		return output.toString();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, ModelMap modelmap) {
		return "home";
	}

	@RequestMapping(value = "/inscription", method = RequestMethod.GET)
	public String inscription(Locale locale, Model model, ModelMap modelmap) {
		List<Club> clubs = clubService.listClubs();
		model.addAttribute("membre", new Membre());
		modelmap.addAttribute("clubs", clubs);

		modelmap.addAttribute("message", true);
		return "inscription";
	}

	@RequestMapping(value = "/inscription", method = RequestMethod.POST)
	public ModelAndView inscrire(@ModelAttribute("command") MembreBean membre,
			BindingResult result, ModelMap map) {
		List<Club> clubs = clubService.listClubs();
		map.addAttribute("clubs", clubs);
		map.addAttribute("message", true);
		Membre m = prepareModel(membre);
		m.setEtat(EtatMembre.ATTENTE);
		sendMail(m, "");

		membreService.createMembre(m);
		ModelAndView res = new ModelAndView("redirect:/inscription");
		res.addObject("message", true);
		return res;
	}

	@RequestMapping(value = "/connexion", method = RequestMethod.GET)
	public String connexion(Locale locale, Model model, ModelMap modelmap) {
		model.addAttribute("membre", new Membre());
		return "connexion";
	}

	@RequestMapping(value = "/connexion", method = RequestMethod.POST)
	public String connecter(HttpSession session,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "mdp") String mdp, ModelMap map) {

		Membre membre = membreService.auth(email, mdp);
		if (membre != null) {
			session.setAttribute("membre", membre);
		} else {
			String messageErreur = "Login ou Password incorrect pour une connexion";
			map.addAttribute("erreur", messageErreur);
		}
		return "home";
	}

	@RequestMapping(value = "/classement/region", method = RequestMethod.GET)
	public String region(Locale locale, Model model, ModelMap modelmap) {
		List<Region> regions = regionService.listRegions();
		modelmap.addAttribute("regions", regions);
		return "region";
	}

	@RequestMapping(value = "/classement/region", method = RequestMethod.POST)
	public String regionClassement(@RequestParam(value = "id") int id,
			Locale locale, Model model, ModelMap modelmap) {
		List<Region> regions = regionService.listRegions();
		modelmap.addAttribute("regions", regions);
		Region reg = regionService.getRegion(id);
		modelmap.addAttribute("reg", reg);
		List<Performance> perfs = performanceService.listPerformances(reg);
		List<Performance> perfs2 = new ArrayList<Performance>();
		Map<Integer, Performance> map = new HashMap<Integer, Performance>();
		for (Performance p : perfs) {
			if (!map.containsKey(p.getMembre().getId())) {
				map.put(p.getMembre().getId(), p);
				perfs2.add(p);
			}
		}
		
		List<PerformanceBean> performances = new ArrayList<PerformanceBean>();
		for (Performance p : perfs2) {
			performances.add(prepareBean(p));
		}
		modelmap.addAttribute("performances", performances);
		return "region";
	}

	@RequestMapping(value = "/classement/competition", method = RequestMethod.GET)
	public String competition(Locale locale, Model model, ModelMap modelmap) {
		List<Competition> competitions = competitionService.listCompetitions();
		modelmap.addAttribute("competitions", competitions);
		return "competition";
	}

	@RequestMapping(value = "/classement/competition", method = RequestMethod.POST)
	public String competitionClassement(@RequestParam(value = "id") int id,
			Locale locale, Model model, ModelMap modelmap) {
		List<Competition> competitions = competitionService.listCompetitions();
		modelmap.addAttribute("competitions", competitions);
		Competition comp = competitionService.getCompetition(id);
		modelmap.addAttribute("comp", comp);
		List<Performance> perfs = performanceService.listPerformances(comp);
		List<Performance> perfs2 = new ArrayList<Performance>();
		Map<Integer, Performance> map = new HashMap<Integer, Performance>();
		for (Performance p : perfs) {
			if (!map.containsKey(p.getMembre().getId())) {
				map.put(p.getMembre().getId(), p);
				perfs2.add(p);
			}
		}
		

		List<PerformanceBean> performances = new ArrayList<PerformanceBean>();
		for (Performance p : perfs2) {
			performances.add(prepareBean(p));
		}
		modelmap.addAttribute("performances", performances);
		return "competition";
	}

	@RequestMapping(value = "/deconnexion", method = RequestMethod.GET)
	public String deconnexion(HttpSession session, Locale locale, Model model,
			ModelMap modelmap) {
		session.removeAttribute("membre");
		return "home";
	}

	@RequestMapping(value = "/performance", method = RequestMethod.GET)
	public String performance(HttpSession session, Locale locale, Model model,
			ModelMap modelmap) {
		List<Competition> competitions = competitionService.listCompetitions();
		modelmap.addAttribute("competitions", competitions);
		modelmap.addAttribute("ajouter", false);
		return "performance";
	}

	@RequestMapping(value = "/performance", method = RequestMethod.POST)
	public String ajoutPerformance(HttpSession session, Locale locale,
			Model model, ModelMap modelmap,
			@RequestParam(value = "date") String date,
			@RequestParam(value = "points") int points,
			@RequestParam(value = "competition") int id,
			@RequestParam(value = "newcompetition") String newcompetition) {
		List<Competition> competitions = competitionService.listCompetitions();
		modelmap.addAttribute("competitions", competitions);

		Competition competition = null;
		if (!newcompetition.isEmpty()) {
			Competition compet = competitionService
					.getCompetition(newcompetition);
			if (compet == null) {
				competitionService.createCompetition(newcompetition);
				competition = competitionService.getCompetition(newcompetition);
				System.out.println("nouvelle competition = "
						+ competition.getNom());
			} else {
				competition = compet;
			}
		}
		Calendar datePerf = Calendar.getInstance();
		try {
			datePerf.setTime(dateFormat.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (id != 0) {
			competition = competitionService.getCompetition(id);
		}
		performanceService.createPerformance(datePerf, points, competition,
				(Membre) session.getAttribute("membre"));
		modelmap.addAttribute("ajouter", true);
		return "performance";
	}

	@RequestMapping(value = "/club/membres", method = RequestMethod.GET)
	public String listeAttente(HttpSession session, Locale locale, Model model,
			ModelMap modelmap) {
		Membre m = (Membre) session.getAttribute("membre");

		List<Membre> tmp = membreService.listMembres(m.getClub());
		List<MembreBean> membres = new ArrayList<MembreBean>();

		for (Membre mbr : tmp) {
			if (mbr.getEtat()!=null && (mbr.getEtat().equals(EtatMembre.REFUSER)
					|| mbr.getEtat().equals(EtatMembre.ATTENTE))) {
				membres.add(prepareBean(mbr));
			}
		}

		modelmap.addAttribute("membres", membres);
		return "club";
	}

	@RequestMapping(value = "/club/membres", method = RequestMethod.POST)
	public String modifStatus(HttpSession session, Locale locale, Model model,
			ModelMap modelmap, @RequestParam(value = "etat") String etat,
			@RequestParam(value = "idMembre") int idMembre,
			@RequestParam(value = "comment") String comment) {

		Membre membreMaj = membreService.getMembre(idMembre);
		membreMaj.setEtat(EtatMembre.valueOf(etat));

		if (etat.equals(EtatMembre.INSCRIT.toString())) {
			membreMaj.setMdp(randomMdp());
			sendMail(membreMaj, comment);
		}

		if (etat.equals(EtatMembre.REFUSER.toString())) {
			sendMail(membreMaj, comment);
		}

		membreService.updateMembre(membreMaj);

		Membre m = (Membre) session.getAttribute("membre");

		List<Membre> tmp = membreService.listMembres(m.getClub());
		List<MembreBean> membres = new ArrayList<MembreBean>();

		for (Membre mbr : tmp) {
			if (mbr.getEtat().equals(EtatMembre.REFUSER)
					|| mbr.getEtat().equals(EtatMembre.ATTENTE)) {
				membres.add(prepareBean(mbr));
			}
		}

		modelmap.addAttribute("membres", membres);
		return "club";
	}
	

	@RequestMapping(value = "/club/membresadmin", method = RequestMethod.GET)
	public String listeAttenteAdmin(HttpSession session, Locale locale, Model model,
			ModelMap modelmap) {

		List<Membre> tmp = membreService.listMembres();
		List<MembreBean> membres = new ArrayList<MembreBean>();

		for (Membre mbr : tmp) {
			if (mbr.getEtat()!=null && mbr.getEtat().equals(EtatMembre.ATTENTE)) {
				membres.add(prepareBean(mbr));
			}
		}

		modelmap.addAttribute("membres", membres);
		return "admin";
	}

	@RequestMapping(value = "/club/membresadmin", method = RequestMethod.POST)
	public String modifStatusAdmin(HttpSession session, Locale locale, Model model,
			ModelMap modelmap, @RequestParam(value = "etat") String etat,
			@RequestParam(value = "idMembre") int idMembre,
			@RequestParam(value = "comment") String comment) {

		Membre membreMaj = membreService.getMembre(idMembre);
		membreMaj.setEtat(EtatMembre.valueOf(etat));

		if (etat.equals(EtatMembre.INSCRIT.toString())) {
			membreMaj.setMdp(randomMdp());
			sendMail(membreMaj, comment);
		}

		if (etat.equals(EtatMembre.REFUSER.toString())) {
			sendMail(membreMaj, comment);
		}

		membreService.updateMembre(membreMaj);


		List<Membre> tmp = membreService.listMembres();
		List<MembreBean> membres = new ArrayList<MembreBean>();

		for (Membre mbr : tmp) {
			if (mbr.getEtat().equals(EtatMembre.REFUSER)
					|| mbr.getEtat().equals(EtatMembre.ATTENTE)) {
				membres.add(prepareBean(mbr));
			}
		}

		modelmap.addAttribute("membres", membres);
		return "admin";
	}
	
	@RequestMapping(value = "/membre/referent", method = RequestMethod.GET)
	public String membreReferent(HttpSession session, Locale locale,
			Model model, ModelMap modelmap) {
		List<Club> clubs = clubService.listClubs();

		modelmap.addAttribute("clubs", clubs);

		return "referent";
	}

	@RequestMapping(value = "/membre/referent", method = RequestMethod.POST)
	public String membreReferent2(HttpSession session, Locale locale,
			Model model, ModelMap modelmap, @RequestParam(value = "id") int id) {
		List<Club> clubs = clubService.listClubs();

		modelmap.addAttribute("clubs", clubs);

		Club club = clubService.getClub(id);
		modelmap.addAttribute("clu", club);

		List<Membre> tmp = membreService.listMembres(club);
		List<MembreBean> membres = new ArrayList<MembreBean>();

		for (Membre mbr : tmp) {
			if (mbr.getEtat() != null
					&& mbr.getEtat().equals(EtatMembre.INSCRIT) ) {
				membres.add(prepareBean(mbr));
			}
		}

		modelmap.addAttribute("membres", membres);
		return "referent";
	}

	@RequestMapping(value = "/membre/referent2", method = RequestMethod.POST)
	public String membreReferent3(HttpSession session, Locale locale,
			Model model, ModelMap modelmap,
			@RequestParam(value = "idref") int idref,
			@RequestParam(value = "idclu") int idclu) {
		List<Club> clubs = clubService.listClubs();

		modelmap.addAttribute("clubs", clubs);

		Club club = clubService.getClub(idclu);
		modelmap.addAttribute("clu", club);

		Membre newRef = membreService.getMembre(idref);
		club.setReferent(newRef);
		clubService.updateClub(club);
		List<Membre> tmp = membreService.listMembres(club);
		List<MembreBean> membres = new ArrayList<MembreBean>();

		for (Membre mbr : tmp) {
			if (mbr.getEtat() != null
					&& (mbr.getEtat().equals(EtatMembre.INSCRIT) || mbr
							.getEtat().equals(EtatMembre.ADMIN))) {
				membres.add(prepareBean(mbr));
			}
		}

		modelmap.addAttribute("membres", membres);
		return "referent";
	}

	public Membre prepareModel(MembreBean membreBean) {
		Membre membre = new Membre();
		membre.setId(membreBean.getId());
		membre.setEmail(membreBean.getEmail());
		membre.setNom(membreBean.getNom());
		membre.setMdp(membreBean.getMdp());
		membre.setPrenom(membreBean.getPrenom());
		membre.setEtat(membreBean.getEtat());
		membre.setClub(clubService.getClub(membreBean.getClub()));
		Calendar date = Calendar.getInstance();
		try {
			date.setTime(dateFormat.parse(membreBean.getDate()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		membre.setDate(date);
		return membre;
	}

	public MembreBean prepareBean(Membre membre) {
		MembreBean res = new MembreBean();
		res.setId(membre.getId());
		res.setNom(membre.getNom());
		res.setPrenom(membre.getPrenom());
		res.setEmail(membre.getEmail());
		res.setMdp(membre.getMdp());
		res.setClub(membre.getClub().getId());
		res.setEtat(membre.getEtat());
		res.setDate(dateFormat.format(membre.getDate().getTime()));
		return res;
	}

	public PerformanceBean prepareBean(Performance performance) {
		PerformanceBean res = new PerformanceBean();
		res.setId(performance.getId());
		res.setMembre(performance.getMembre());
		res.setPoints(performance.getPoints());
		res.setDate(dateFormat.format(performance.getDate().getTime()));
		return res;
	}

	public void sendMail(Membre membre, String comment) {

		System.out.println("PASSAGE FCT MAIL OK");

		final String username = "master2eservicemakusa@gmail.com";
		final String password = "Patlabor13";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		String msgBody = "";
		if (membre.getEtat().equals(EtatMembre.ATTENTE)) {
			msgBody += "Bonjour " + membre.getPrenom() + " " + membre.getNom()
					+ ",\n\n";
			msgBody += "Votre demande d'inscription a été prise en compte. Elle sera traiter prochainement par le référent du club de "
					+ membre.getClub().getNom() + ".\n\n";
			msgBody += "Cordialement \nBenjamin Makusa Master 2 E-service";
			System.out.println("PASSAGE ATTENTE");
		} else if (membre.getEtat().equals(EtatMembre.INSCRIT)) {
			msgBody += "Bonjour " + membre.getPrenom() + " " + membre.getNom()
					+ ",\n\n";
			msgBody += "Félicitation votre demande d'inscription a été valider par les administrateurs du club. Vous pouvez dès à présent vous connecter sur le portail avec pour email "
					+ membre.getEmail()
					+ " et mot de passe "
					+ membre.getMdp()
					+ "\n\n";
			msgBody += "Cordialement \nBenjamin Makusa Master 2 E-service";
			System.out.println("PASSAGE INSCRIT");
		} else if (membre.getEtat().equals(EtatMembre.REFUSER)) {
			msgBody += "Bonjour " + membre.getPrenom() + " " + membre.getNom()
					+ ",\n\n";
			msgBody += "Je suis dans le regret de vous annoncer que votre demande d'inscription à été refuser.\n Raison :\n"
					+ comment + "\n\n";
			msgBody += "Cependant votre inscription peut être valider ultérieurement\n\n";
			msgBody += "Cordialement \nBenjamin Makusa Master 2 E-service";
			System.out.println("PASSAGE REFUSER");
		}

		System.out.println(msgBody);
		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("master2eservicemakusa@gmail.com",
					" Admin"));
			msg.addRecipient(Message.RecipientType.TO,
					new InternetAddress(membre.getEmail(), membre.getPrenom()
							+ " " + membre.getNom()));
			msg.setSubject("Confirmation de votre demande d'inscription");
			msg.setText(msgBody);
			Transport.send(msg);

		} catch (AddressException e) {
			System.out.println("exception 1");
		} catch (MessagingException e) {
			System.out.println("exception 2 \n" + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			System.out.println("exception 3");
		}
	}

}
