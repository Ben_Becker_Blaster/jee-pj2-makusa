package com.portailsport.bean;

import java.util.List;

public class RegionBean {

	private int id;
	private String nom;
	private List<ClubBean> clubs;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<ClubBean> getClubs() {
		return clubs;
	}
	public void setClubs(List<ClubBean> clubs) {
		this.clubs = clubs;
	}
}
