package com.portailsport.bean;

import java.util.List;

public class ClubBean {

	private int id;
	private String nom;
	private RegionBean region;
	private MembreBean referent;
	private List<MembreBean> membres;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public RegionBean getRegion() {
		return region;
	}

	public void setRegion(RegionBean region) {
		this.region = region;
	}

	public MembreBean getReferent() {
		return referent;
	}

	public void setReferent(MembreBean referent) {
		this.referent = referent;
	}

	public List<MembreBean> getMembres() {
		return membres;
	}

	public void setMembres(List<MembreBean> membres) {
		this.membres = membres;
	}
}
