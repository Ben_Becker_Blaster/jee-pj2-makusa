package com.portailsport.bean;

import java.util.Calendar;

import com.portailsport.model.Membre;

public class PerformanceBean {
	
	private int id;
	private String date;
	private int points;
	private Membre membre;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public Membre getMembre() {
		return membre;
	}
	public void setMembre(Membre membre) {
		this.membre = membre;
	}
}
