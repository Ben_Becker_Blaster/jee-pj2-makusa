package com.portailsport.bean;

import java.util.Calendar;

import com.portailsport.model.Club;
import com.portailsport.model.EtatMembre;

public class MembreBean {
	
	private int id;
	private String nom;
	private String prenom;
	private String email;
	private String date;
	private String mdp;
	private EtatMembre etat;
	private int club;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public EtatMembre getEtat() {
		return etat;
	}
	public void setEtat(EtatMembre etat) {
		this.etat = etat;
	}
	public int getClub() {
		return club;
	}
	public void setClub(int club) {
		this.club = club;
	}
	
	

}
