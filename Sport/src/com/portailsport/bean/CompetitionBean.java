package com.portailsport.bean;

import java.util.List;


public class CompetitionBean {

	private int id;
	private String nom;
	private List<PerformanceBean> performances;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<PerformanceBean> getPerformances() {
		return performances;
	}
	public void setPerformances(List<PerformanceBean> performances) {
		this.performances = performances;
	}
}
