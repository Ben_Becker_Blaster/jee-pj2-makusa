package com.portailsport.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "performance")
public class Performance  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6869067603712792148L;

	@Id
	@GeneratedValue
	@Column(name = "performance_id")
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "performance_date")
	private Calendar date;
	
	@Column(name = "performance_points")
	private int points;

	@ManyToOne
    @JoinColumn(name = "membre_id")
    private Membre membre;
	
	@ManyToOne
	@JoinColumn(name = "competition_id")
    private Competition competition;

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Membre getMembre() {
		return membre;
	}

	public void setMembre(Membre membre) {
		this.membre = membre;
	}
	
}
