package com.portailsport.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "region")
public class Region implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1781488294025042803L;

	@Id
	@GeneratedValue
	@Column(name = "region_id")
	private int id;
	
	@Column(name = "region_nom")
	private String nom;
	
	@OneToMany
	private List<Club> clubs;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Club> getClubs() {
		return clubs;
	}

	public void setClubs(List<Club> clubs) {
		this.clubs = clubs;
	}
	
}
