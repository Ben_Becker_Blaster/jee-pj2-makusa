package com.portailsport.model;

public enum EtatMembre {
	INSCRIT,
	REFUSER,
	ATTENTE,
	ADMIN
}
