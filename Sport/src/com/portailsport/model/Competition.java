package com.portailsport.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "competition")
public class Competition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5550959226008771740L;

	@Id
	@GeneratedValue
	@Column(name = "competition_id")
	private int id;

	@Column(name = "competition_nom")
	private String nom;
	
	@OneToMany
	private List<Performance> performances;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Performance> getPerformances() {
		return performances;
	}

	public void setPerformances(List<Performance> performances) {
		this.performances = performances;
	}
	
	public String toString(){
		return nom;
	}
}
