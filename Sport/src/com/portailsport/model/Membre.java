package com.portailsport.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "membre")
public class Membre implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3528824725289665648L;

	@Id
	@GeneratedValue
	@Column(name = "membre_id")
	private int id;
	
	@Column(name = "membre_nom")
	private String nom;
	
	@Column(name = "membre_prenom")
	private String prenom;

	@Column(name = "membre_email")
	private String email;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "membre_naissance")
	private Calendar date;
	
	@Column(name = "membre_mdp")
	private String mdp;

	@Column(name = "membre_etat")
	@Enumerated(EnumType.STRING)
	private EtatMembre etat;

	@ManyToOne
    @JoinColumn(name = "club_id")
    private Club club;

	@OneToMany
	private List<Performance> performances;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Calendar getDate() {
		System.out.println("Date = "+date);
		return date;
	}

	public void setDate(Calendar date) {
		System.out.println("Date = "+date);
		this.date = date;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public EtatMembre getEtat() {
		return etat;
	}

	public void setEtat(EtatMembre etat) {
		this.etat = etat;
	}

	public Club getClub() {
		System.out.println("Club = "+club);
		return club;
	}

	public void setClub(Club club) {
		System.out.println("Club = "+club);
		this.club = club;
	}

	public List<Performance> getPerformances() {
		return performances;
	}

	public void setPerformances(List<Performance> performances) {
		this.performances = performances;
	}	
	
}
