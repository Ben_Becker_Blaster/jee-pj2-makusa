package com.portailsport.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "club")
public class Club implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2436833671666421085L;

	@Id
	@GeneratedValue
	@Column(name = "club_id")
	private int id;
	
	@Column(name = "club_nom")
	private String nom;
	
	@ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
	
	@ManyToOne
    @JoinColumn(name = "membre_id")
    private Membre referent;
	
	@OneToMany
	private List<Membre> membres;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Membre getReferent() {
		return referent;
	}

	public void setReferent(Membre referent) {
		this.referent = referent;
	}

	public List<Membre> getMembres() {
		return membres;
	}

	public void setMembres(List<Membre> membres) {
		this.membres = membres;
	}

	public String toString(){
		return nom;
	}
}
