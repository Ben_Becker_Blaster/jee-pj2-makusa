CREATE DATABASE  IF NOT EXISTS `sport` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sport`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: sport
-- ------------------------------------------------------
-- Server version	5.6.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `club`
--

DROP TABLE IF EXISTS `club`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `club` (
  `club_id` int(11) NOT NULL AUTO_INCREMENT,
  `club_nom` varchar(255) DEFAULT NULL,
  `membre_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`club_id`),
  KEY `FK2EA4B6E9C082F` (`region_id`),
  KEY `FK2EA4B65259E8AF` (`membre_id`),
  CONSTRAINT `FK2EA4B65259E8AF` FOREIGN KEY (`membre_id`) REFERENCES `membre` (`membre_id`),
  CONSTRAINT `FK2EA4B6E9C082F` FOREIGN KEY (`region_id`) REFERENCES `region` (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `club`
--

LOCK TABLES `club` WRITE;
/*!40000 ALTER TABLE `club` DISABLE KEYS */;
INSERT INTO `club` VALUES (1,'Lille',2,1),(2,'Lens',NULL,1),(3,'Paris',NULL,3),(4,'Lyon',NULL,6),(5,'Renne',NULL,7),(6,'Nante',NULL,7),(7,'Bordeau',NULL,2);
/*!40000 ALTER TABLE `club` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `club_membre`
--

DROP TABLE IF EXISTS `club_membre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `club_membre` (
  `club_club_id` int(11) NOT NULL,
  `membres_membre_id` int(11) NOT NULL,
  UNIQUE KEY `membres_membre_id` (`membres_membre_id`),
  KEY `FK8C194BC9ED6087A6` (`club_club_id`),
  KEY `FK8C194BC92BF9CFC3` (`membres_membre_id`),
  CONSTRAINT `FK8C194BC92BF9CFC3` FOREIGN KEY (`membres_membre_id`) REFERENCES `membre` (`membre_id`),
  CONSTRAINT `FK8C194BC9ED6087A6` FOREIGN KEY (`club_club_id`) REFERENCES `club` (`club_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `club_membre`
--

LOCK TABLES `club_membre` WRITE;
/*!40000 ALTER TABLE `club_membre` DISABLE KEYS */;
/*!40000 ALTER TABLE `club_membre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition` (
  `competition_id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`competition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competition`
--

LOCK TABLES `competition` WRITE;
/*!40000 ALTER TABLE `competition` DISABLE KEYS */;
INSERT INTO `competition` VALUES (1,'Basket'),(2,'Football'),(3,'Tennis'),(4,'Handball'),(5,'Rugby'),(6,'Volley'),(7,'KickBoxing');
/*!40000 ALTER TABLE `competition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competition_performance`
--

DROP TABLE IF EXISTS `competition_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition_performance` (
  `competition_competition_id` int(11) NOT NULL,
  `performances_performance_id` int(11) NOT NULL,
  UNIQUE KEY `performances_performance_id` (`performances_performance_id`),
  KEY `FK45559B021ED3361` (`performances_performance_id`),
  KEY `FK45559B016BA1205` (`competition_competition_id`),
  CONSTRAINT `FK45559B016BA1205` FOREIGN KEY (`competition_competition_id`) REFERENCES `competition` (`competition_id`),
  CONSTRAINT `FK45559B021ED3361` FOREIGN KEY (`performances_performance_id`) REFERENCES `performance` (`performance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competition_performance`
--

LOCK TABLES `competition_performance` WRITE;
/*!40000 ALTER TABLE `competition_performance` DISABLE KEYS */;
/*!40000 ALTER TABLE `competition_performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre`
--

DROP TABLE IF EXISTS `membre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membre` (
  `membre_id` int(11) NOT NULL AUTO_INCREMENT,
  `membre_naissance` datetime DEFAULT NULL,
  `membre_email` varchar(255) DEFAULT NULL,
  `membre_etat` varchar(255) DEFAULT NULL,
  `membre_mdp` varchar(255) DEFAULT NULL,
  `membre_nom` varchar(255) DEFAULT NULL,
  `membre_prenom` varchar(255) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`membre_id`),
  KEY `FKBFC28C202EDF522F` (`club_id`),
  CONSTRAINT `FKBFC28C202EDF522F` FOREIGN KEY (`club_id`) REFERENCES `club` (`club_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre`
--

LOCK TABLES `membre` WRITE;
/*!40000 ALTER TABLE `membre` DISABLE KEYS */;
INSERT INTO `membre` VALUES (1,'2015-05-19 01:15:07','ADMIN','ADMIN','ADMIN','Makusa','Benjamin',1),(2,'2015-05-19 01:15:07','cyrdikilu@hotmail.fr','INSCRIT','a?xh1ayiqXb','Dikilu','Cyril',1),(3,'2015-05-19 01:15:07','jerrobillard@hotmail.fr','REFUSER',NULL,'Robillard','Jerome',1),(4,'2015-05-19 01:15:07','jersamsom@hotmail.fr','ATTENTE',NULL,'Samsom','Jeremie',2),(5,'2015-05-19 01:15:07','reymarzak@hotmail.fr','ATTENTE',NULL,'Marzak','Reynald',2),(6,'2015-05-19 01:15:07','oscgest@hotmail.fr','INSCRIT','test','Gest','Oscar',3),(7,'2015-05-19 01:15:07','benmakusa@hotmail.fr','INSCRIT','Azerty@123','Makusa','Benjamin',1),(8,'2014-07-15 00:00:00','jmlepen@gaming.fr','REFUSER',NULL,'Skywalker','Luc',5);
/*!40000 ALTER TABLE `membre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_performance`
--

DROP TABLE IF EXISTS `membre_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membre_performance` (
  `membre_membre_id` int(11) NOT NULL,
  `performances_performance_id` int(11) NOT NULL,
  UNIQUE KEY `performances_performance_id` (`performances_performance_id`),
  KEY `FK2760CA9121ED3361` (`performances_performance_id`),
  KEY `FK2760CA911D34C750` (`membre_membre_id`),
  CONSTRAINT `FK2760CA911D34C750` FOREIGN KEY (`membre_membre_id`) REFERENCES `membre` (`membre_id`),
  CONSTRAINT `FK2760CA9121ED3361` FOREIGN KEY (`performances_performance_id`) REFERENCES `performance` (`performance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_performance`
--

LOCK TABLES `membre_performance` WRITE;
/*!40000 ALTER TABLE `membre_performance` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performance`
--

DROP TABLE IF EXISTS `performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `performance` (
  `performance_id` int(11) NOT NULL AUTO_INCREMENT,
  `performance_date` datetime DEFAULT NULL,
  `performance_points` int(11) DEFAULT NULL,
  `performance_type` varchar(255) DEFAULT NULL,
  `membre_id` int(11) DEFAULT NULL,
  `competition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`performance_id`),
  KEY `FKA7C310305259E8AF` (`membre_id`),
  CONSTRAINT `FKA7C310305259E8AF` FOREIGN KEY (`membre_id`) REFERENCES `membre` (`membre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performance`
--

LOCK TABLES `performance` WRITE;
/*!40000 ALTER TABLE `performance` DISABLE KEYS */;
INSERT INTO `performance` VALUES (1,'2015-05-20 23:41:11',10,NULL,1,1),(2,'2015-05-20 23:41:11',15,NULL,1,2),(3,'2015-05-20 23:41:11',30,NULL,2,3),(4,'2015-05-20 23:41:11',40,NULL,3,3),(5,'2015-05-20 23:41:11',5,NULL,4,4),(6,'2015-05-20 23:41:11',10,NULL,5,5),(7,'2015-06-16 00:00:00',236,NULL,2,7);
/*!40000 ALTER TABLE `performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region_nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'Nord-Pas-De-Calais'),(2,'Picardie'),(3,'Alsace-Lorraine'),(4,'Ile-De-France'),(5,'Gironde'),(6,'Normandie'),(7,'Bretagne'),(8,'Aquitaine'),(9,'Loire-Atlantique');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region_club`
--

DROP TABLE IF EXISTS `region_club`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region_club` (
  `region_region_id` int(11) NOT NULL,
  `clubs_club_id` int(11) NOT NULL,
  UNIQUE KEY `clubs_club_id` (`clubs_club_id`),
  KEY `FKA2A76F21A0A808A4` (`region_region_id`),
  KEY `FKA2A76F21626D1E6D` (`clubs_club_id`),
  CONSTRAINT `FKA2A76F21626D1E6D` FOREIGN KEY (`clubs_club_id`) REFERENCES `club` (`club_id`),
  CONSTRAINT `FKA2A76F21A0A808A4` FOREIGN KEY (`region_region_id`) REFERENCES `region` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region_club`
--

LOCK TABLES `region_club` WRITE;
/*!40000 ALTER TABLE `region_club` DISABLE KEYS */;
/*!40000 ALTER TABLE `region_club` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-05 23:26:37
